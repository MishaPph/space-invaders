﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Libs
{
    public class PoolObjects : MonoBehaviour
    {
        private readonly Dictionary<string, GameObject> _prefabs = new Dictionary<string, GameObject>();
        private readonly Dictionary<string, Queue<GameObject>> _active = new Dictionary<string, Queue<GameObject>>();
        
        public static PoolObjects Active { get; private set; }

        private void Awake()
        {
            Active = this;
        }

        public void Register(string key, GameObject prefab, int amountPreload)
        {
            _prefabs.Add(key, prefab);
            var q = new Queue<GameObject>(amountPreload);
            for (var i = 0; i < amountPreload; i++)
            {
                var obj = Instantiate(prefab, transform);
                obj.name = key;
                obj.gameObject.SetActive(false);
                q.Enqueue(obj);
            }
            _active.Add(key, q);
        }

        public void Return(GameObject obj)
        {
            if (_active.TryGetValue(obj.name, out var queue))
            {
                obj.SetActive(false);
                obj.transform.SetParent(transform);
                queue.Enqueue(obj);
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public GameObject Get(string key)
        {
            if (_active.TryGetValue(key, out var queue) && queue.Count > 0)
            {
                var obj = queue.Dequeue();
                obj.transform.SetParent(null ,false);
                obj.SetActive(true);
                return obj;
            }
            if (_prefabs.TryGetValue(key, out var prefab))
            {
                var go= Instantiate(prefab);
                go.name = key;
                return go;
            }
            throw new ArgumentException();
        }

        public T Get<T>(string key) where T : Object
        {
            return Get(key).GetComponent<T>();
        }
    }
}
