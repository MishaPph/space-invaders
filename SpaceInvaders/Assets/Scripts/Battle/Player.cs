﻿using System;
using Libs;
using UnityEngine;

namespace Battle
{
    public class Player : MonoBehaviour, IDamageable
    {
        private float _horizontalLimit;
        public float Speed = 8.0f;
        public event Action OnHit;
        public bool IsDeath { get; private set; }
        public Vector2 Size => transform.localScale * GetComponent<BoxCollider2D>().size;
        
        public void Spawn(Vector3 position, float horizontalLimit)
        {
            gameObject.SetActive(true);
            transform.position = position;
            IsDeath = false;
            _horizontalLimit = horizontalLimit;
        }
        
        public void Damage()
        {
            OnHit?.Invoke();
            gameObject.SetActive(false);
            IsDeath = true;
        }

        public void Fire()
        {
            var bullet = PoolObjects.Active.Get<Bullet>("Bullet");
            bullet.transform.position = transform.position;
            bullet.Play(Vector2.up);
        }

        public void MoveBy(float delta)
        {
            var pos = transform.position;
            transform.position = new Vector3(Mathf.Clamp(pos.x + delta, -_horizontalLimit, _horizontalLimit), pos.y, pos.z);
        }
        
        private void Update()
        {
            if (!IsDeath)
            {
                MoveBy(Input.GetAxis("Horizontal") * Time.deltaTime * Speed);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Fire();
                }
            }
        }
    }
}
