﻿using System;
using Battle;
using Libs;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamageable
{
    [SerializeField] private EnemyData _data;
    public int Reward => _data.Reward;
    public bool IsRightDirection { get; private set; } = true;
    public float CurrentSpeed { get; private set; }

    private Action<bool> _hitBlockRight;
    private Action<Enemy> _die;
    
    private void Start()
    {
        if (_data.FireRate > 0)
        {
            InvokeRepeating(nameof(Fire), _data.FireRate, _data.FireRate);
        }
    }

    public void Set(bool moveToRight, Action<bool> hitRight, Action<Enemy> onDie)
    {
        CurrentSpeed = _data.Speed;
        IsRightDirection = moveToRight;
        _hitBlockRight = hitRight;
        _die = onDie;
    }

    public void SetDirection(bool right)
    {
        IsRightDirection = right;
    }

    public void StepDown(float value, float speedInc)
    {
        CurrentSpeed += speedInc;
        transform.position += new Vector3(0, -value, 0);
    }
    
    public void Update()
    {
        transform.position += new Vector3((IsRightDirection?CurrentSpeed:-CurrentSpeed)*Time.deltaTime, 0,0);
    }

    public virtual void Damage()
    {
        _die?.Invoke(this);
        Destroy(gameObject, 0.1f);
    }
    
    protected virtual void OnCollisionEnter2D(Collision2D other)
    {
        var t = other.gameObject.tag;
        if (t.Equals("LeftBorder"))
        {
            _hitBlockRight?.Invoke(false);
        }
        else  if (t.Equals("RightBorder"))
        {
            _hitBlockRight?.Invoke(true);
        }
    }

    private void Fire()
    {
        var bullet = PoolObjects.Active.Get<Bullet>(_data.BulletPrefab.name);
        bullet.transform.position = transform.position;
        bullet.Play(Vector2.down);
    }
}
