﻿using System;
using UnityEngine;

namespace Battle
{
    public class MotherShip : Enemy
    {
        public void Set(bool moveToRight, Action<Enemy> onDie)
        {
            base.Set(moveToRight, null, onDie);
        }

        protected override void OnCollisionEnter2D(Collision2D other)
        {
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var t = other.gameObject.tag;
            if ((t.Equals("LeftBorder") && !IsRightDirection) || (t.Equals("RightBorder") && IsRightDirection))
            {
                Destroy(gameObject);
            }
        }
    }
}
