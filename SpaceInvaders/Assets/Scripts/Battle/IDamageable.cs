﻿namespace Battle
{
    public interface IDamageable
    {
        void Damage();
    }
}