﻿using UnityEngine;

namespace Battle
{
   [CreateAssetMenu(fileName = "EnemyData", menuName = "EnemyData")]
   public class EnemyData : ScriptableObject
   {
      public int Reward = 1;
      public int FireRate = 0;
      public float Speed = 1;
      
      public GameObject BulletPrefab;
   }
}
