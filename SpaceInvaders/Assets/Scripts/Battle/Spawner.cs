﻿using System;
using Libs;
using UnityEngine;

namespace Battle
{
    public class Spawner : MonoBehaviour
    {
        public Enemy enemyPrefab;
        public Enemy enemyFirePrefab;
        public MotherShip motherShipPrefab;
        
        public GameObject playerBulletPrefab;
        public GameObject enemyBulletPrefab;
        
        public Player playerPrefab;

        public Enemy SpawnEnemy(Vector3 position, bool fire)
        {
            var enemy = Instantiate(fire?enemyFirePrefab:enemyPrefab);
            enemy.transform.position = position;
            return enemy;
        }
        
        public MotherShip SpawnMotherShip()
        {
            var enemy = Instantiate(motherShipPrefab);
            return enemy;
        }
        
        public Player SpawnPlayer()
        {
            var obj = Instantiate(playerPrefab);
            return obj;
        }
        
        public Bullet SpawnEnemyBullet(Vector3 position)
        {
            var go = Instantiate(enemyBulletPrefab, position, Quaternion.identity);
            return go.GetComponent<Bullet>();
        }
    }
}