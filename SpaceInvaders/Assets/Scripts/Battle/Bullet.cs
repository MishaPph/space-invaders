﻿using Libs;
using UnityEngine;

namespace Battle
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D _rigidbody;
       
        public void Play(Vector2 velocity)
        {
            _rigidbody.AddForce(velocity*10, ForceMode2D.Impulse);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            var damageable = other.GetComponent<IDamageable>();
            damageable?.Damage();
            PoolObjects.Active.Return(gameObject);
        }
    }
}
