﻿using System.Collections.Generic;
using Battle;
using UnityEngine;

public class EnemyController:MonoBehaviour
{
    [SerializeField] private Transform _rightBlock;
    [SerializeField] private Transform _leftBlock;
    
    public float MotherShipRepeat = 4.0f;
    public float MotherShipSpawnPercent = 0.4f;
    
    public Vector2 EnemyCellSize = new Vector2(0.8f,0.72f);
    
    public Vector2Int EnemyCount = new Vector2Int(6,6);
    
    private readonly List<Enemy> _enemies = new List<Enemy>();

    private MotherShip _motherShip;
    private GameInstance _game;

    public int CountLiveEnemies => _enemies.Count;
    public bool HaveLiveEnemies => CountLiveEnemies > 0;
    public bool MotherShipLive => _motherShip != null;

    public void Initialize(GameInstance gameInstance)
    {
        _game = gameInstance;
        _leftBlock.position = new Vector3(-_game.LevelWidth,0,0);
        _rightBlock.position = new Vector3(_game.LevelWidth,0,0);
    }
    
    public void Setup()
    {
        SpawnEnemies(_game.LevelHeight);
        InvokeRepeating(nameof(SpawnMotherShip), MotherShipRepeat, MotherShipRepeat);
    }

    public void StopSpawn()
    {
        CancelInvoke();
    }
   
    private void SpawnEnemies(float topPoint)
    {
        var size = EnemyCellSize;
        topPoint -= size.y;
        topPoint -= size.y/2;
            
        for (var i = 0; i < EnemyCount.x; i++)
        {
            for (var j = 0; j < EnemyCount.y; j++)
            {
                var isFireEnemy = (j == 1 || j == 3) && (i == 0 || i == 5);
                var position = new Vector3(size.x / 2 - size.x * (EnemyCount.x/2.0f) + i * size.x, topPoint - j * size.y, 0);
                var enemy = _game.Spawner.SpawnEnemy(position, isFireEnemy);
                enemy.Set(true, EnemyHitBlockRight, EnemyDied);
                _enemies.Add(enemy);
            }
        }
    }

    private void SpawnMotherShip()
    {
        if(_motherShip != null)
            return;
        if(Random.Range(0.0f, 1.0f) > MotherShipSpawnPercent)
            return;
      
        var moveToRight = Random.Range(0.0f, 1.0f) < 0.5f;
        _motherShip = _game.Spawner.SpawnMotherShip();
        _motherShip.transform.position = new Vector3(_game.LevelWidth * ( moveToRight?-1:1), _game.LevelHeight - EnemyCellSize.y/2, 0);
        _motherShip.Set(moveToRight, EnemyDied);
    }
   
    private void EnemyDied(Enemy obj)
    {
        _enemies.Remove(obj);
        if (obj == _motherShip)
        {
            _motherShip = null;
        }

        _game.EnemyKilled(obj.transform.position, obj.Reward);
    }
      
    private void EnemyHitBlockRight(bool right)
    {
        var newDirection = !right;
        foreach (var enemy in _enemies)
        {
            if(enemy.IsRightDirection == newDirection)
                return;
            enemy.SetDirection(newDirection);
            enemy.StepDown(0.1f, 0.1f);
        }
    }
}