﻿using UnityEngine;

public static class LayerHelper
{
    static LayerHelper()
    {
        PlayerBulletId = LayerMask.NameToLayer("PlayerBullet");
        EnemyBulletId = LayerMask.NameToLayer("EnemyBullet");
        EnemyId = LayerMask.NameToLayer("Enemy");
        PlayerId = LayerMask.NameToLayer("Player");
    }

    public static int EnemyBulletId { get; private set; }
    public static int PlayerBulletId { get; private set; }
    public static int EnemyId { get; private set; }
    public static int PlayerId { get; set; }
}
