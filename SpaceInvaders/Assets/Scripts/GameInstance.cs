﻿using System;
using System.Collections;
using Battle;
using Libs;
using UI;
using UnityEngine;

public class GameInstance : MonoBehaviour
{
   [SerializeField] private BattleUI _battleUi;

   [SerializeField] private Camera _camera;
   [SerializeField] private Spawner _spawner;

   [SerializeField] private EnemyController _enemyController;
   public Spawner Spawner => _spawner;
   public int Score { get; private set; }

   public int Life { get; private set; } = 3;

   public float LevelWidth => _camera.aspect * _camera.orthographicSize;
   public float LevelHeight => _camera.orthographicSize;

   private Player _player;

   private void Awake()
   {
      _enemyController.Initialize(this);
   }

   private void Start()
   {
      PoolObjects.Active.Register("Bullet", _spawner.playerBulletPrefab, 3);
      PoolObjects.Active.Register("EnemyBullet", _spawner.enemyBulletPrefab, 5);
      
      Life = 3;
      Score = 0;
      _battleUi.Init(Life);
      
      CreatePlayer();
      
      _enemyController.Setup();
      
      StartCoroutine(CoolDown(3, null));
   }

   public void EnemyKilled(Vector3 position, int score)
   {
      AddScore(position, score);
      if (!_enemyController.HaveLiveEnemies && !_enemyController.MotherShipLive)
      {
         GameFinish(true);
      }
   }

   private void AddScore(Vector3 position, int score)
   {
      Score += score;
      _battleUi.SetScore(Score);
      _battleUi.PlayScore( _camera.WorldToViewportPoint(position), score);
   }
   
   private void GameFinish(bool win)
   {
      _enemyController.StopSpawn();
      PopupController.Active.Show<FinishGamePopup>().Set(win);
   }

   private void CreatePlayer()
   {
      _player = _spawner.SpawnPlayer();
      _player.OnHit += OnPlayerHit;
      SpawnPlayer();
   }
   
   private void SpawnPlayer()
   {
      _player.Spawn(new Vector3(0,-LevelHeight + _player.Size.x/2,0), LevelWidth);
   }
   
   private void OnPlayerHit()
   {
      Life--;
      _battleUi.ShowLives(Life);
      if (Life > 0)
      {
         StartCoroutine(CoolDown(3, SpawnPlayer));
      }
      else
      {
         GameFinish(false);
      }
   }
   
   
   private IEnumerator CoolDown(int seconds,  Action end)
   {
      Time.timeScale = 0.0f;
      for (var i = 0; i < seconds; i++)
      {
         _battleUi.ShowCoolDown((seconds - i).ToString());
         yield return new WaitForSecondsRealtime(1.0f);
      }
      _battleUi.HideCoolDown();
      Time.timeScale = 1.0f;
      end?.Invoke();
   }
}
