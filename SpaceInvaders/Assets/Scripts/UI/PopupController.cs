﻿using UnityEngine;

namespace UI
{
    public class PopupController : MonoBehaviour
    {
        public static PopupController Active { get; private set; }
        public void Awake()
        {
            Active = this;
        }

        public T Show<T>() where T: MonoBehaviour
        {
            return Instantiate(Resources.Load<T>("Popups/" + typeof(T).Name), transform);
        }
    }
}
