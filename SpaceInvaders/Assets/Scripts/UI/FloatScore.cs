﻿using Libs;
using TMPro;
using UnityEngine;

namespace UI
{
    public class FloatScore: MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;

        public void Set(string text)
        {
            _text.text = text;
            Invoke(nameof(Hide), 0.5f);
        }

        private void Hide()
        {
            PoolObjects.Active.Return(gameObject);
        }
    }
}