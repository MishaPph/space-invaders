﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenuScreen : MonoBehaviour
    {
        public void Play()
        {
            SceneManager.LoadScene("Battle");
        }
    }
}
