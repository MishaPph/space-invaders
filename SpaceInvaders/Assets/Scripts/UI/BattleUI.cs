﻿using Libs;
using TMPro;
using UnityEngine;

namespace UI
{
    public class BattleUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _score;
        
        [SerializeField] private TextMeshProUGUI _coolDown;
        
        [SerializeField] private Transform _livePanel;
        [SerializeField] private GameObject _livePrefab;
        
        [SerializeField] private GameObject _floatScorePrefab;
        
        private GameObject[] _lives;

        public void Init(int lives)
        {
            foreach (Transform child in _livePanel)
            {
                Destroy(child.gameObject);
            }
            _lives = new GameObject[lives];
            for (var i = 0; i < lives; i++)
            {
                _lives[i] = Instantiate(_livePrefab, _livePanel);
            }
            SetScore(0);
            PoolObjects.Active.Register("FloatScore", _floatScorePrefab, 5);
        }
        
        public void SetScore(int score)
        {
            _score.text = score.ToString();
        }

        public void ShowLives(int count)
        {
            for (var i = 0; i < _lives.Length; i++)
            {
                _lives[i].SetActive(i < count);
            }
        }

        public void ShowCoolDown(string text)
        {
            _coolDown.text = text;
        }

        public void HideCoolDown()
        {
            _coolDown.text = "";
        }

        public void PlayScore(Vector2 screenPos, int count)
        {
            var size= ((RectTransform) transform.parent).sizeDelta;
            
           var fl= PoolObjects.Active.Get<FloatScore>("FloatScore");
           fl.Set(count.ToString());
           fl.transform.SetParent(transform, true);
           
           screenPos.Scale(size);
           fl.GetComponent<RectTransform>().anchoredPosition = screenPos;
        }
    }
}
