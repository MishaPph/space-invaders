﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishGamePopup : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _caption;

    public void Set(bool win)
    {
        _caption.text = win ? "you win!!!" : "you lose";
    }

    public void Restart()
    {
        SceneManager.LoadScene("Battle");
    }
    
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
