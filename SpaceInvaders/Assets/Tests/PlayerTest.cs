﻿using System.Collections;
using Battle;
using Libs;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class PlayerTest : MonoBehaviour
    {
        
        private PoolObjects poolObjects;
        private Spawner spawner;
        [SetUp]
        public void Setup()
        {
            var go = new GameObject("PoolObjects");
            poolObjects = go.AddComponent<PoolObjects>();
            spawner = Resources.Load<Spawner>("Spawner");
        }

        [TearDown]
        public void Teardown()
        {
            Destroy(poolObjects.gameObject);
            spawner = null;
        }
        
        [TestCase(50, ExpectedResult = 50 )]
        [TestCase(-30, ExpectedResult = -30 )]
        public float MoveBy_WhenAddedDelta_ShouldChangePosition(float value)
        {
            var player = spawner.SpawnPlayer();
            player.Spawn(Vector3.zero, 1000);
            player.MoveBy(value);
            var x = player.transform.position.x;
            Destroy(player.gameObject);
            return x;
        }
        
        [TestCase(150, ExpectedResult = 100 )]
        [TestCase(-30, ExpectedResult = -30 )]
        [TestCase(-330, ExpectedResult = -100 )]
        public float MoveBy_WhenAddedDelta_ShouldClampPosition(float value)
        {
            var player = spawner.SpawnPlayer();
            player.Spawn(Vector3.zero, 100);
            player.MoveBy(value);
            var x = player.transform.position.x;
            Destroy(player.gameObject);
            return x;
        }
        
        [Test]
        public void Damage_ShouldCallHit()
        {
            var player = spawner.SpawnPlayer();
            player.Spawn(Vector3.zero, 100);

            var changed = false;
            player.OnHit += () =>
            {
                changed = true;
            };
            
            player.Damage();
            
            Destroy(player.gameObject);
            Assert.That(changed, Is.EqualTo(true));
        }
        
        [Test]
        public void Damage_ShouldDeath()
        {
            var player = spawner.SpawnPlayer();
            player.Spawn(Vector3.zero, 100);
            player.Damage();
            var death = player.IsDeath;
            Destroy(player.gameObject);
            
            Assert.That(death, Is.EqualTo(true));
        }
        
        [Test]
        public void Spawn_ShouldRestore()
        {
            var player = spawner.SpawnPlayer();
            player.Spawn(Vector3.zero, 100);
            player.Damage();
            player.Spawn(Vector3.zero, 100);
            var life =  !player.IsDeath;
            Destroy(player.gameObject);
            
            Assert.That(life, Is.EqualTo(true));
        }

        [UnityTest]
        public IEnumerator WhenSpawnEnemyBullet_PlayerShouldDie()
        {
            var player = spawner.SpawnPlayer();
            poolObjects.Register("EnemyBullet", spawner.enemyBulletPrefab, 1);
            player.Spawn(Vector3.zero, 100);

            var bullet = poolObjects.Get<Bullet>("EnemyBullet");
            bullet.transform.position = player.transform.position;
            yield return new WaitForSeconds(0.2f);
            var death = player.IsDeath;
            Destroy(player.gameObject);
            
            Assert.That(death, Is.EqualTo(true));
        }
    }
}
