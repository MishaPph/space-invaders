﻿using System.Collections;
using Libs;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class PoolObjectsTest
    {
        
        private PoolObjects poolObjects;

        [SetUp]
        public void Setup()
        {
            var go = new GameObject("PoolObjects");
            poolObjects = go.AddComponent<PoolObjects>();
        }

        [TearDown]
        public void Teardown()
        {
            Object.Destroy(poolObjects.gameObject);
        }
        
        [Test]
        public void Get_WhenQueIsEmpty_ShouldReturnNew()
        {
            poolObjects.Register("Tmp", new GameObject("Test"), 0);

            var test = poolObjects.Get("Tmp");
            Assert.IsNotNull(test);
        }

        [Test]
        public void Get_WhenObjectNotRegistered_ShouldReturnException()
        {
            Assert.That(()=> poolObjects.Get("Tmp2"), Throws.ArgumentException);
        }
    }
}
